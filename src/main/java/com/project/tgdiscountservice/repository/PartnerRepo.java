package com.project.tgdiscountservice.repository;

import com.project.tgdiscountservice.model.Partner;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface PartnerRepo extends JpaRepository<Partner, Long> {

    @Query("select p from Partner p where p.admitadId in (:ids)")
    @EntityGraph(value = "Partner[coupons, categories]")
    List<Partner> findByAdmitadIds(Collection<Long> ids);

    @Query("select p from Partner p where p.admitadId = :id")
    Optional<Partner> findByAdmitadId(Long id);

    @Query("select p from Partner p")
    @EntityGraph(value = "Partner[coupons, categories]")
    List<Partner> findAll();

    @Query("select p from Partner p where p.id = :id")
    @EntityGraph(value = "Partner[coupons]")
    Optional<Partner> findById(Long id);
}
