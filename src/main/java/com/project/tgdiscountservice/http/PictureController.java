package com.project.tgdiscountservice.http;

import com.project.tgdiscountservice.model.Partner;
import com.project.tgdiscountservice.repository.PartnerRepo;
import com.project.tgdiscountservice.service.SVGToJpegConverter;
import com.project.tgdiscountservice.service.partner.PartnerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("pictures")
public class PictureController {

    private final PartnerService partnerService;
    private final SVGToJpegConverter svgToJpegConverter;


    @GetMapping(value = "/{partnerId}", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody
    byte[] getPicture(@PathVariable Long partnerId) throws IOException {
        log.info("PictureController getPicture - {}", partnerId);
        Partner partner = partnerService.findPartner(partnerId);
        return svgToJpegConverter.transcodeSVGToBufferedImage(partner.getImageUrl());
    }

}
