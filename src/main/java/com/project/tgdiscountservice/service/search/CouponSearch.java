package com.project.tgdiscountservice.service.search;


import com.project.tgdiscountservice.model.Coupon;
import com.project.tgdiscountservice.model.Request;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.search.engine.backend.analysis.AnalyzerNames;
import org.hibernate.search.engine.search.query.SearchResult;
import org.hibernate.search.mapper.orm.Search;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class CouponSearch  {

    private final EntityManager entityManager;

    public List<Coupon> search(Request request) {
        SearchSession session = Search.session(entityManager);
        SearchResult<Coupon> result = session.search(Coupon.class)
                .where(find -> find.match()
                        .fields("description", "name", "shortName")
                        .matching(request.getWord())
                        .analyzer(AnalyzerNames.DEFAULT))
                .fetch(50);

        List<Coupon> coupons = result.total().hitCount() == 0 ? Collections.emptyList() : result.hits()
                .stream()
                .filter(this::dateIsAfterToday)
                .collect(Collectors.toList());
        return coupons;
    }

    private boolean dateIsAfterToday(Coupon coupon) {
        if (coupon.getDateEnd() == null) {
            coupon.setDateEnd(LocalDateTime.of(2500, 10, 10, 10, 10, 10));
        }
        return coupon.getDateEnd().isAfter(LocalDateTime.now());
    }

}
