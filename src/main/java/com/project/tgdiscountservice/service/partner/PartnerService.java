package com.project.tgdiscountservice.service.partner;

import com.project.tgdiscountservice.model.Partner;
import com.project.tgdiscountservice.repository.PartnerRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class PartnerService {

    private final PartnerRepo partnerRepo;

    public Partner findPartner(Long id) {
        return partnerRepo.findById(id).orElseThrow(RuntimeException::new);
    }

    public Partner save(Partner partner) {
        return partnerRepo.save(partner);
    }

    public List<Partner> findAll() {
        return partnerRepo.findAll();
    }

    public List<Partner> findByAdmitadIds(Collection<Long> ids) {
        return partnerRepo.findByAdmitadIds(ids);
    }

    public Optional<Partner> findByAdmitadId(Long id) {
        return partnerRepo.findByAdmitadId(id);
    }

    public List<Partner> saveAll(Collection<Partner> partners) {
        return partnerRepo.saveAll(partners);
    }
}
