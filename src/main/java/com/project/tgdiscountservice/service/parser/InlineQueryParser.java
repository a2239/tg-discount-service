package com.project.tgdiscountservice.service.parser;

import com.project.tgdiscountservice.model.inner.InnerUpdate;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@Data
public class InlineQueryParser extends Parser {

    private static final String TYPE_RESOLVER = "inlineQuery";

    @Override
    public ParseData parse(InnerUpdate update) {
        log.info("InlineQueryParser parse - {}", update);
        ParseData data = new ParseData();
        data.setInlineQuery(update.getInlineQuery());
        data.setQuery(data.getInlineQuery().getQuery());
        data.setChatId(Long.valueOf(data.getInlineQuery().getId()));
        //TODO replace to string to config variables
        data.setCommand("/query");
        return data;
    }

    @Override
    public String getCommand() {
        return TYPE_RESOLVER;
    }
}
