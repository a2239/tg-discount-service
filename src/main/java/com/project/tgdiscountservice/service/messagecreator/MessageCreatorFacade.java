package com.project.tgdiscountservice.service.messagecreator;

import com.project.tgdiscountservice.model.inner.InnerUpdate;
import com.project.tgdiscountservice.service.parser.ParseData;
import com.project.tgdiscountservice.service.parser.Parser;
import com.project.tgdiscountservice.service.parser.ParserRecognizer;
import com.project.tgdiscountservice.service.parser.factories.MessageCreatorFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MessageCreatorFacade {

    private final MessageCreatorFactory messageCreatorFactory;
    private final ParserRecognizer recognizer;

    public void prepareMessage(InnerUpdate update) {
        log.info("MessageCreatorFacade prepareMessage - {}", update);
        Parser parser = recognizer.recognize(update);
        ParseData data = parser.parse(update);
        String command = data.getCommand();
        MessageCreator creator = messageCreatorFactory.getCreator(command);
        creator.create(update, data);
    }
}
